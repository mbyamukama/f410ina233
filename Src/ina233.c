#include "ina233.h"
#include <stdbool.h>
#include <math.h>


extern I2C_HandleTypeDef hi2c1;

uint8_t INA233_IsDeviceReady(uint8_t addr) {
  ina233_i2caddr = addr<<1;
  m_c=0;
  R_c=0;
  m_p=0;
  R_p=0;
  return HAL_I2C_IsDeviceReady(&hi2c1, ina233_i2caddr, 2, 0xffffffff);
}

void wireSendCmd(uint8_t reg)
{
	//wireWriteByte(reg , NULL);
}
void wireWriteByte(uint8_t reg, uint16_t data)
{
	uint8_t i2c_temp[1];
	i2c_temp[0] = data ;
	HAL_I2C_Mem_Write(&hi2c1, ina233_i2caddr, (uint16_t)reg, 1, i2c_temp, 1,0xffffffff);
	HAL_Delay(1);
}


void wireReadByte(uint8_t reg, uint8_t *data)
{
	HAL_I2C_Mem_Read(&hi2c1, ina233_i2caddr, (uint16_t)reg, 1, data, 1,0xffffffff);
	HAL_Delay(1);
}

void wireWriteRegister (uint8_t reg, uint16_t data)
{
	uint8_t i2c_temp[2];
	i2c_temp[0] =  data & 0xFF;
	i2c_temp[1] = (data >> 8) & 0xFF;//try interchanging
	HAL_I2C_Mem_Write(&hi2c1, ina233_i2caddr, (uint16_t)reg, 1, i2c_temp, 2,0xffffffff);
	HAL_Delay(1);

}

void wireReadRegister(uint8_t reg, uint16_t *data)
{
	HAL_I2C_Mem_Read(&hi2c1, ina233_i2caddr, (uint16_t)reg, 1, data, 2,0xffffffff);
	HAL_Delay(1);
}


/**************************************************************************/
/*!
    @brief  Set INA233 Calibration register for measuring based on the user's
    inputs r_shunt and i_max.
    -inputs: value of the shunt resistor and maximum current (in ohms and A)
    -inputs as outputs: measuring accuracy for current (uA) and power (mW) and
    ERROR state for possible errors during Calibration.
    -outputs: the CAL value to be written in MFR_CALIBRATION
    */
/**************************************************************************/
uint16_t setCalibration(float r_shunt,float i_max,float *Current_LSB,float *Power_LSB, int16_t *mc,int8_t *Rc, int16_t *mp, int8_t *Rp,  uint8_t *ERROR)
{
  float C_LSB=0;
  float P_LSB=0;
  float CAL=0;
  float m_c_F=0;
  float m_p_F=0;
  int32_t aux=0;
  bool round_done=false;
  int8_t local_R_c=0;
  int8_t local_R_p=0;
  uint8_t local_ERROR=0;

  C_LSB=i_max/pow(2,15);
  P_LSB=25*C_LSB;
  *Current_LSB=C_LSB*1000000;
  *Power_LSB=P_LSB*1000;
  CAL=0.00512/(r_shunt*C_LSB);

  //Check CAL is in the uint16 range
  if (CAL>0xFFFF)
    {
      local_ERROR=1;
    }
  else
    {
    wireWriteRegister(MFR_CALIBRATION, (uint16_t)CAL);
    }
  m_c_F=1/C_LSB;
  m_p_F=1/P_LSB;

  //Calculate m and R for maximum accuracy in current measurement
  aux=(int32_t)m_c_F;
  while ((aux>32768)||(aux<-32768))
    {
      m_c_F=m_c_F/10;
      local_R_c++;
      aux=(int32_t)m_c_F;
    }
  while (round_done==false)
    {
      aux=(int32_t)m_c_F;
      if (aux==m_c_F)
      {
        round_done=true;
      }
      else
      {
         aux=(int32_t)(m_c_F*10);             //shift decimal to the right
         if ((aux>32768)||(aux<-32768))       //m_c is out of int16 (-32768 to 32768)
         {
          round_done=true;
         }
         else
         {
          m_c_F=m_c_F*10;
          local_R_c--;
         }
      }
    }
  round_done=false;
  //Calculate m and R for maximum accuracy in power measurement
  aux=(int32_t)m_p_F;
  while ((aux>32768)||(aux<-32768))
    {
      m_p_F=m_p_F/10;
      local_R_p++;
      aux=(int32_t)m_p_F;
    }
  while (round_done==false)
    {
      aux=(int32_t)m_p_F;
      if (aux==m_p_F)
      {
        round_done=true;
      }
      else
      {
         aux=(int32_t)(m_p_F*10);          //shift decimal to the right
         if ((aux>32768)||(aux<-32768))       //m_p is out of int16 (-32768 to 32768)
         {
          round_done=true;
         }
         else
         {
          m_p_F=m_p_F*10;
          local_R_p--;
         }
      }
    }
  *mp=m_p_F;
  *mc=m_c_F;
  *Rc=local_R_c;
  *Rp=local_R_p;
  *ERROR=local_ERROR;

  m_c=(int16_t)m_c_F;
  m_p=(int16_t)m_p_F;
  R_c=local_R_c;
  R_p=local_R_p;

  return(uint16_t)CAL;
}

/**************************************************************************/
/*!
    @brief  Gets the raw bus voltage (2-byte, two's complement integer
    received from the device)
*/
/**************************************************************************/
int16_t getBusVoltage_raw() {
  uint16_t value;
  wireReadRegister(READ_VIN, &value);
  return (int16_t)value;
}
/**************************************************************************/
/*!
    @brief  Gets the raw shunt voltage (2-byte, two's complement integer
    received from the device)
*/
/**************************************************************************/
int16_t getShuntVoltage_raw() {
  uint16_t value;
  wireReadRegister(MFR_READ_VSHUNT, &value);
  return (int16_t)value;
}
/**************************************************************************/
/*!
    @brief  Gets the raw current value (2-byte, two's complement integer
    received from the device)
*/
/**************************************************************************/
int16_t getCurrent_raw() {
  uint16_t value;
  // Now we can safely read the CURRENT register!
  wireReadRegister(READ_IIN, &value);

  return (int16_t)value;
}
/**************************************************************************/
/*!
    @brief  Gets the raw power value (2-byte, two's complement integer
    received from the device)
*/
/**************************************************************************/
int16_t getPower_raw() {
  uint16_t value;
  // Now we can safely read the POWER register!
  wireReadRegister(READ_PIN, &value);

  return (int16_t)value;
}


/**************************************************************************/
/*!
    @brief  Gets the shunt voltage in mV
*/
/**************************************************************************/
float getShuntVoltage_mV() {
  uint16_t value=getShuntVoltage_raw();
  float vshunt;
  vshunt=(value*pow(10,-R_vs)-b_vs)/m_vs;
  return vshunt * 1000;
}

/**************************************************************************/
/*!
    @brief  Gets the shunt voltage in volts
*/
/**************************************************************************/
float getBusVoltage_V() {
  uint16_t value=getBusVoltage_raw();
  float vbus;
  vbus =(value*pow(10,-R_vb)-b_vb)/m_vb;
  return vbus;
}

/**************************************************************************/
/*!
    @brief  Gets the current value in mA, taking into account the
            config settings and current LSB
*/
/**************************************************************************/
float getCurrent_mA() {
  uint16_t value=getCurrent_raw();
  float current;
  current =(value*pow(10,-R_c)-b_c)/m_c;
  return current*1000;
}

/**************************************************************************/
/*!
    @brief  Gets the power value in mW, taking into account the
            config settings and power LSB
*/
/**************************************************************************/
float getPower_mW() {
  uint16_t value=getPower_raw();
  float power;
  power =(value*pow(10,-R_p)-b_p)/m_p;
  return power*1000;
}
